export const products = [
    {
        id: 1,
        title: "Cabral",
        price: "1,080.00",
        image: "images/Products/Condolencias/img1.jpg",
        imageHover: "/images/Products/Condolencias/img-hover1.jpg",
        quickView: "/images/Products/Condolencias/quick-view-img1.jpg"
    },
    {
        id: 2,
        title: "Sabina",
        price: "1,435.00",
        image: "images/Products/Condolencias/img2.jpg",
        imageHover: "/images/Products/Condolencias/img-hover2.jpg",
        quickView: "images/Products/Condolencias/img2.jpg"
    },{
        id: 3,
        title: "Sant",
        price: "1,500.00",
        image: "images/Products/Condolencias/img3.jpg",
        imageHover: "/images/Products/Condolencias/img-hover3.jpg",
        quickView: "images/Products/Condolencias/img3.jpg"
    },{
        id: 4,
        title: "Neruda",
        price: "990.00",
        image: "images/Products/Condolencias/img4.jpg",
        imageHover: "/images/Products/Condolencias/img-hover4.jpg",
        quickView: "images/Products/Condolencias/img4.jpg"
    },
    {
        id: 5,
        title: "Galeano",
        price: "715.00",
        image: "images/Products/Condolencias/img5.jpg",
        imageHover: "/images/Products/Condolencias/img-hover5.jpg",
        quickView: "images/Products/Condolencias/img5.jpg"
    },{
        id: 6,
        title: "Vega",
        price: "$1,080.00",
        image: "images/Products/Condolencias/img6.jpg",
        imageHover: "/images/Products/Condolencias/img-hover6.jpg",
        quickView: "images/Products/Condolencias/img6.jpg"
    },{
        id: 7,
        title: "Marlin",
        price: "1,080.00",
        image: "images/Products/Condolencias/img7.jpg",
        imageHover: "/images/Products/Condolencias/img-hover7.jpg",
        quickView: "images/Products/Condolencias/img7.jpg"
    },
]