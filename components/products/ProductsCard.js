import React, { Component } from 'react';
import Link from 'next/link';
import AddToCart from '../Shared/AddToCart';
import AddToCompare from '../Shared/AddToCompare';

class ProductsCard extends Component {
    state = {
        modalOpen: false,
        modalData: null
    };
    openModal = () => {
        this.setState({ modalOpen: true });
    }

    closeModal = () => {
        this.setState({ modalOpen: false });
    }

    compareButton = (id) => {
        let compare_exist = this.props.CompareProducts.filter(item => item.id === id);
        if(compare_exist.length > 0){
            return(
                <Link href="#">
                    <a 
                        data-tip="Already Added" 
                        data-place="left" 
                        onClick={e => {
                                e.preventDefault(); 
                            }
                        }
                        disabled={true}
                        className="disabled"
                    >
                        <i className="fas fa-sync"></i>
                    </a>
                </Link>
            )
        } else {
            return(
                <AddToCompare id={id} />
            )
        }
    }

    handleModalData = (data) => {
        this.setState({ 
            modalData: data
        });
    }

    render() {
        let { products } = this.props;
        const { modalOpen } = this.state;
        return (
            <>
                {products.map((data, idx) => (
                    <div className="col-lg-4 col-sm-6 col-md-4 col-6 products-col-item" key={idx}>
                        <div className="single-product-box">
                            <div className="product-image">
                                <Link href="/product/[id]" as={`/product/${data.producto}`}>
                                    <a>
                                        <img src={data.foto} alt="image" />
                                        <img src={data.foto} alt="image" />
                                    </a>
                                </Link>

                                
                            </div>

                            <div className="product-content">
                                <h3>
                                    <Link href="/product/[id]" as={`/product/${data.producto}`}>
                                        <a>{data.producto}</a>
                                    </Link>
                                </h3>

                                <div className="product-price">
                                    <span className="new-price">${data.precio}</span>
                                </div>
                                <AddToCart {...data} />
                            </div>
                        </div>
                    </div>
                ))}
                { modalOpen ? <QuickView 
                    closeModal={this.closeModal} 
                    modalData={this.state.modalData}
                /> : '' }
            </>
        );
    }
}

export default ProductsCard
