import React, { Component } from 'react';
import Link from 'next/link';

class AddsModal extends Component {
    _isMounted = false;
    state = {
        open: false
    };

    componentDidMount(){
        this.setState({
            open: true
        });
    }

    closeModal = (e) => {
        this._isMounted = true;
        e.preventDefault();
        this.setState({
            open: false
        });
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    render() {
        let { open } = this.state;
        return (
            <div className={`bts-popup ${open ? 'is-visible' : ''}`} role="alert">
                <div className="bts-popup-container">
                    <h3>Envio gratis</h3>
                    <p>Coviertete en un miembro y tendras envios gratis en tu primera compra</p>

                    <form>
                        <input type="email" className="form-control" placeholder="Correo" name="Correo" required={true} />
                        <button type="submit"><i className="far fa-paper-plane"></i></button>
                    </form>

                    

                    <div className="img-box">
                        <img src="/images/flower-bouquet-tubes-fleur-bouquets-wedding-pink-rose-bouquet-10.png" alt="image" />
                        <img src="/images/flor 1.png" alt="image" />
                    </div>

                    <Link href="#">
                        <a onClick={this.closeModal} className="bts-popup-close"></a>
                    </Link>
                </div>
            </div>
        );
    }
}

export default AddsModal;
