import React, { Component } from 'react';
import { connect } from 'react-redux'
import Link from 'next/link';

class Cart extends Component {

    state = {
        display: false,
        products: [],
        total: 0
    };

    closeCart = () => {
        this.props.onClick(this.state.display);
    }

    componentDidMount() {
        const products = JSON.parse(localStorage.getItem('carrito'));

        console.log(products);
        if (products) {
            let total = 0;
            products.forEach(product => {
                console.log(product.precio);
                total += product.precio * product.qty;
            });
            console.log(this.state);
            const aux = this.state
            this.setState({ ...aux, products, total });
        }
    }

    async pay() {

        fetch('/api/stripe', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                products: this.state.products,
                total: this.state.total
            })
        }).then(res => res.json()).then(data => {
            window.location.href = data.url;
        }).catch(err => {
            console.log(err);
        })

    }

    render() {
        return (
            <div
                className="modal right fade show shoppingCartModal"
                style={{
                    display: "block", paddingRight: "16px"
                }}
            >
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <button
                            type="button"
                            className="close"
                            data-dismiss="modal"
                            aria-label="Close"
                            onClick={this.closeCart}
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <div className="modal-body">
                            <h3>Mi carrito ({this.state.products.length})</h3>

                            <div className="product-cart-content">

                                {this.state.products.length > 0 ? this.state.products.map((product, idx) => (
                                    <div className="product-cart" key={idx}>
                                        <div className="product-image">
                                            <img src={product.foto} alt="image" />
                                        </div>

                                        <div className="product-content">
                                            <h3>
                                                <Link href="#">
                                                    <a>{product.producto}</a>
                                                </Link>
                                            </h3>

                                            <div className="product-price">
                                                <span>{product.qty}</span>
                                                <span>x</span>
                                                <span className="price">${product.precio}</span>
                                            </div>
                                        </div>
                                    </div>
                                )) : 'Vacio'}

                            </div>

                            <div className="product-cart-subtotal">
                                <span>Subtotal</span>

                                <span className="subtotal">${this.state.total}</span>
                            </div>

                            <div className="product-cart-btn" onClick={this.pay.bind(this)}>
                                <p className="btn btn-primary">Proceder a pago</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.addedItems,
        total: state.total
        //addedItems: state.addedItems
    }
}

export default connect(mapStateToProps)(Cart)