import React, { Component } from 'react';
import Link from 'next/link';

class ProductsFilterOptions extends Component {

    handleGrid = (evt, e) => {
        this.props.onClick(e);
        let i, aLinks;

        aLinks = document.getElementsByTagName("a");
        for (i = 0; i < aLinks.length; i++) {
            aLinks[i].className = aLinks[i].className.replace("active", "");
        }

        evt.currentTarget.className += " active";
    }

    render() {
        return (
            <div className="products-filter-options">
                <div className="row align-items-center">
                    <div className="col d-flex">
                        <span>Vista:</span>

                        <div className="view-list-row">
                            <div className="view-column">
                                <Link href="#">
                                    <a 
                                        className="icon-view-two"
                                        onClick={e => {
                                            e.preventDefault();
                                            this.handleGrid(e, "products-col-two")
                                        }}
                                    >
                                        <span></span>
                                        <span></span>
                                    </a>
                                </Link>

                                <Link href="#">
                                    <a 
                                        className="icon-view-three active"
                                        onClick={e => {
                                            e.preventDefault();
                                            this.handleGrid(e, "")
                                        }}
                                    >
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </a>
                                </Link>

                                <Link href="#">
                                    <a 
                                        className="icon-view-four"
                                        onClick={e => {
                                            e.preventDefault();
                                            this.handleGrid(e, "products-col-four")
                                        }}
                                    >
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </a>
                                </Link>

                                <Link href="#">
                                    <a 
                                        className="view-grid-switch"
                                        onClick={e => {
                                            e.preventDefault();
                                            this.handleGrid(e, "products-row-view")
                                        }}
                                    >
                                        <span></span>
                                        <span></span>
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>

                    <div className="col d-flex justify-content-center">
                        <p>Mostrando 22 of 102 Resultados</p>
                    </div>

                    <div className="col d-flex">
                        <span>Mostrar:</span>

                        <div className="show-products-number">
                            <select>
                                <option value="1">22</option>
                                <option value="2">32</option>
                                <option value="3">42</option>
                                <option value="4">52</option>
                                <option value="5">62</option>
                            </select>
                        </div>

                        <span>Acomodar:</span>

                        <div className="products-ordering-list">
                            <select>
                                <option value="1">Featured</option>
                                <option value="2">Mas vendidos</option>
                                <option value="3">Menor precio</option>
                                <option value="4">Mayor valor</option>
                                <option value="5">Mas recinetes</option>
                                <option value="6">Mas antiguos</option>
                                <option value="7">Orden Acendente</option>
                                <option value="8">Orden decendente</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductsFilterOptions;
