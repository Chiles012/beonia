import React, { Component } from 'react';
import Link from "next/link";

class Footer extends Component {
    render() {
        return (
            <footer className="footer-area">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-md-6">
                            <div className="single-footer-widget">
                                <div className="logo">
                                    <Link href="/">
                                        <a>
                                            BLOOM1
                                        </a>
                                    </Link>
                                </div>

                                <p>
                                 LAlalalaa
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-6">
                            <div className="single-footer-widget">
                                <h3>Links</h3>
                                <ul className="quick-links">
                                    <li>
                                        <Link href="/about">
                                            <a>Sobre Nosotros</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="/faq">
                                            <a>Faq's</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="/customer-service">
                                            <a>servicio al cliente</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="/contact-us">
                                            <a>Contactanos</a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-6">
                            <div className="single-footer-widget">
                                <h3>Información</h3>

                                <ul className="information-links">
                                    <li>
                                        <Link href="/about">
                                            <a>ARREGLOS</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="/contact-us">
                                            <a>MOTIVOS</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="/sizing-guide">
                                            <a>COMPLEMENTOS</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="/customer-service">
                                            <a>EVENTOS</a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-6">
                            <div className="single-footer-widget">
                                <h3>Contactanos</h3>

                                <ul className="footer-contact-info">
                                    <li>
                                        <i className="fas fa-map-marker-alt"></i> 
                                        Ubicanos: 2750 Quadra Street <br /> Irapuato, Mexico
                                    </li>
                                    <li>
                                        <i className="fas fa-phone"></i> 
                                        Call Us: <a href="tel:(+123) 456-7898">(442) 456-7898</a>
                                    </li>
                                    <li>
                                        <i className="far fa-envelope"></i> 
                                        Correo: <a href="mailto:support@novine.com">support@bloom.com</a>
                                    </li>
                                    <li>
                                        <i className="fas fa-fax"></i> 
                                        Telefono: <a href="tel:+123456">442-44550454</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="copyright-area">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-6 col-md-6">
                                <p>Copyright &copy; 2021 BLOOM. Todos los derechos reservados a <a href="https://sapphiredevelopment.com.mx//" target="_blank">Sapphire Dev</a></p>
                            </div>

                            <div className="col-lg-6 col-md-6">
                                <ul className="payment-card">
                                  
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
