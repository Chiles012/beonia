import React, { Component } from 'react';
import { connect } from 'react-redux';
import Link from 'next/link';
import Cart from '../Modal/Cart';

class MegaMenu extends Component {

    state = {
        display: false,
        searchForm: false,
        collapsed: true
    };

    handleCart = () => {
        this.setState(prevState => {
            return {
                display: !prevState.display
            };
        });
    }

    handleSearchForm = () => {
        this.setState(prevState => {
            return {
                searchForm: !prevState.searchForm
            };
        });
    }

    toggleNavbar = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    componentDidMount() {
        let elementId = document.getElementById("navbar");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId.classList.add("is-sticky");
            } else {
                elementId.classList.remove("is-sticky");
            }
        });
        window.scrollTo(0, 0);
    }

    render() {
        const { collapsed } = this.state;
        const classOne = collapsed ? 'collapse navbar-collapse' : 'collapse navbar-collapse show';
        const classTwo = collapsed ? 'navbar-toggler navbar-toggler-right collapsed' : 'navbar-toggler navbar-toggler-right';

        return (
            <>
                <div className="navbar-area">
                    <div id="navbar" className="comero-nav">
                        <div className="container">
                            <nav className="navbar navbar-expand-md navbar-light">
                                <Link href="/">
                                    <a className="navbar-brand">
                                    <img src="/images/MG4@4x.png" className="logo-img" alt="image" />
                                    </a>
                                </Link>

                                <button
                                    onClick={this.toggleNavbar}
                                    className={classTwo}
                                    type="button"
                                    data-toggle="collapse"
                                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false"
                                    aria-label="Toggle navigation"
                                >
                                    <span className="icon-bar top-bar"></span>
                                    <span className="icon-bar middle-bar"></span>
                                    <span className="icon-bar bottom-bar"></span>
                                </button>

                                <div className={classOne} id="navbarSupportedContent">
                                    <ul className="navbar-nav">
                                        <li className="nav-item p-relative">


                                            <Link href="about">
                                                <a className="nav-link active">
                                                    ¿QUIÉNES SOMOS?
                                                </a>
                                            </Link>


                                        </li>

                                        <li className="nav-item megamenu">
                                            <Link href="#">
                                                <a className="nav-link" onClick={e => e.preventDefault()}>
                                                    ARREGLOS <i className="fas fa-chevron-down"></i>
                                                </a>
                                            </Link>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item">
                                                    <div className="container">
                                                        <div className="row">
                                                            <div className="col">
                                                                <h6 className="submenu-title">Arreglos</h6>

                                                                <ul className="megamenu-submenu">
                                                                    <li>
                                                                        <Link href="">
                                                                            <a>Ramos</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="">
                                                                            <a>Jarones</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="">
                                                                            <a>Canastas</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="">
                                                                            <a>Cajas y bases</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="">
                                                                            <a>Tazas</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="">
                                                                            <a>Con vino o cerveza </a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="">
                                                                            <a>Con dulces</a>
                                                                        </Link>
                                                                    </li>

                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>

                                        <li className="nav-item megamenu">
                                            <Link href="#">
                                                <a className="nav-link" onClick={e => e.preventDefault()}>
                                                    MOTIVOS <i className="fas fa-chevron-down"></i>
                                                </a>
                                            </Link>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item">
                                                    <div className="container">
                                                        <div className="row">
                                                            <div className="col">
                                                                <h6 className="submenu-title">Todos</h6>

                                                                <ul className="megamenu-submenu">
                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Amor/Aniversario</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Cumpleaños</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Para ellas</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Dresses</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Para ellos</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Graduación</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Condolencias</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>T-Baby shower/Bienvenida Bebé </a>
                                                                        </Link>
                                                                    </li>
                                                                </ul>
                                                            </div>






                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>

                                        <li className="nav-item megamenu">
                                            <Link href="#">
                                                <a className="nav-link" onClick={e => e.preventDefault()}>COMPLEMENTOS<i className="fas fa-chevron-down"></i></a>
                                            </Link>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item">
                                                    <div className="container">
                                                        <div className="row">
                                                            <div className="col">
                                                                <h6 className="submenu-title">Todos</h6>

                                                                <ul className="megamenu-submenu">
                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Fresas</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Globos</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Pasteles y galletas</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Peluches</a>
                                                                        </Link>
                                                                    </li>

                                                                    <li>
                                                                        <Link href="/view-products">
                                                                            <a>Chocolates</a>
                                                                        </Link>
                                                                    </li>


                                                                </ul>
                                                            </div>





                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>

                                        <li className="nav-item megamenu">
                                            <Link href="/">
                                                <a className="nav-link active">
                                                    EVENTOS
                                                </a>
                                            </Link>
                                        </li>

                                        <li className="nav-item p-relative">
                                            <Link href="/">
                                                <a className="nav-link active">
                                                    FAVORITOS
                                                </a>
                                            </Link>


                                        </li>

                                        <li className="nav-item p-relative">
                                            <Link href="contact-us">
                                                <a className="nav-link active">
                                                    CONTACTO
                                                </a>
                                            </Link>


                                        </li>
                                    </ul>

                                    <div className="others-option">
                                        <div className="option-item">
                                            <i
                                                onClick={this.handleSearchForm}
                                                className="search-btn fas fa-search"
                                                style={{
                                                    display: this.state.searchForm ? 'none' : 'block'
                                                }}
                                            ></i>

                                            <i
                                                onClick={this.handleSearchForm}
                                                className={`close-btn fas fa-times ${this.state.searchForm ? 'active' : ''}`}
                                            ></i>

                                            <div
                                                className="search-overlay search-popup"
                                                style={{
                                                    display: this.state.searchForm ? 'block' : 'none'
                                                }}
                                            >
                                                <div className='search-box'>
                                                    <form className="search-form">
                                                        <input className="search-input" name="search" placeholder="Search" type="text" />
                                                        <button className="search-button" type="submit">
                                                            <i className="fas fa-search"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="option-item">
                                            <i
                                                style={{
                                                    cursor: 'pointer'
                                                }}
                                                onClick={this.handleCart}
                                                className="fas fa-shopping-bag"
                                            ></i>
                                        </div>

                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>

                {this.state.display ? <Cart onClick={this.handleCart} /> : ''}
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.addedItems
    }
}

export default connect(mapStateToProps)(MegaMenu)
