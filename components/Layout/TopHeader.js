import React, { Component } from 'react';
import Link from 'next/link';


class TopHeader extends Component {

    state = {
        display: false
    };

    handleWishlist = () => {
        this.setState( prevState => {
            return {
                display: !prevState.display
            };
        });
    }

    render() {
        return (
            <>
                <div className="top-header">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-7 col-md-6">
                               
                            </div>

                            <div className="col-lg-5 col-md-6">
                                <ul className="top-header-right-nav">
                                   
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

               
            </>
        );
    }
}

export default TopHeader;