import React, { Component } from 'react';
import Link from 'next/link';

class LeftSidebar extends Component {
    state = {
    
        collection: false,
     
    }

    handleToggle = (e, evt) => {
        e.preventDefault();

       if (evt == "collection"){
            this.setState({
                collection: !this.state.collection
            })
        } 
    }

    render() {
        let {  collection } = this.state;
        return (
            <div className={`col-lg-${this.props.col ? this.props.col : '4' } col-md-12`}>
                <div className="woocommerce-sidebar-area">
                    

                    <div 
                        className={`collapse-widget collections-list-widget ${collection ? '' : 'open'}`}
                    >
                        <h3 
                            className={`collapse-widget-title ${collection ? '' : 'active'}`}
                            onClick={e => this.handleToggle(e, "collection")}
                        >
                            Flor

                            <i className="fas fa-angle-up"></i>
                        </h3>

                        <ul className={`collections-list-row ${collection ? 'block' : 'none'}`}>
                            <li className="active">
                                <Link href="#">
                                    <a>Rosas</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>Gerberas</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>Girasoles</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>Lilis</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>Lishuantus</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="#">
                                    <a>Margaritas</a>
                                </Link>
                            </li>
                        </ul>
                    </div>

                    

                    

                    

                    

                    

                    <div className="collapse-widget aside-products-widget">
                        <h3 className="aside-widget-title">
                            Productos populares
                        </h3>

                        <div className="aside-single-products">
                            <div className="products-image">
                                <Link href="#">
                                    <a>
                                        <img src="/images/img1.jpg" alt="image" />
                                    </a>
                                </Link>
                            </div>

                            <div className="products-content">
                                <span>
                                    <Link href="#">
                                        <a>Arreglo</a>
                                    </Link>
                                </span>
                                <h3>
                                    <Link href="#">
                                        <a>Cabral</a>
                                    </Link>
                                </h3>

                                <div className="product-price">
                                    <span className="new-price">$1,380.00</span>
                                    <span className="old-price">$1,080.00</span>
                                </div>
                            </div>
                        </div>

                        <div className="aside-single-products">
                            <div className="products-image">
                                <Link href="#">
                                    <a>
                                        <img src="/images/img1.jpg" alt="image" />
                                    </a>
                                </Link>
                            </div>

                            <div className="products-content">
                                <span>
                                    <Link href="#">
                                        <a>Arreglo</a>
                                    </Link>
                                </span>
                                <h3>
                                    <Link href="#">
                                        <a>Cabral</a>
                                    </Link>
                                </h3>

                                <div className="product-price">
                                    <span className="new-price">$$1,380.00</span>
                                    <span className="old-price">$1,080.00</span>
                                </div>
                            </div>
                        </div>

                        <div className="aside-single-products">
                            <div className="products-image">
                                <Link href="#">
                                    <a>
                                        <img src="/images/img1.jpg" alt="image" />
                                    </a>
                                </Link>
                            </div>

                            <div className="products-content">
                                <span>
                                    <Link href="#">
                                        <a>Arreglo</a>
                                    </Link>
                                </span>
                                <h3>
                                    <Link href="#">
                                        <a>Cabral</a>
                                    </Link>
                                </h3>

                                <div className="product-price">
                                    <span className="new-price">$1,080.00</span>
                                    <span className="old-price">$1,080.00</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="collapse-widget aside-trending-widget">
                        <div className="aside-trending-products">
                            <img src="/images/bestseller-hover-img1.jpg" alt="image" />

                            <div className="category">
                                <h4>En tendencia</h4>
                                <span>San valentin</span>
                            </div>
                            <Link href="#">
                                <a></a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LeftSidebar;
