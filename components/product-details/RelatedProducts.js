import React, { Component } from 'react';
import Link from 'next/link';
import dynamic from 'next/dynamic';
const OwlCarousel = dynamic(import('react-owl-carousel3'));

import AddToCart from '../Shared/AddToCart';

const options = {
    loop: true,
    nav: false,
    dots: true,
    margin: 30,
    autoplayHoverPause: true,
    autoplay: true,
    navText: [
        "<i class='fas fa-chevron-left'></i>",
        "<i class='fas fa-chevron-right'></i>"
    ],
    responsive: {
        0: {
            items: 1,
        },
        576: {
            items: 2,
        },
        768: {
            items: 2,
        },
        1024: {
            items: 3,
        },
        1200: {
            items: 4,
        }
    }
}

class RelatedProducts extends Component {
    state = { 
        display: false,
        modalOpen: false,
        modalData: null
    };

    componentDidMount(){ 
        this.setState({ display: true }) 
    }

    openModal = () => {
        this.setState({ modalOpen: true });
    }

    closeModal = () => {
        this.setState({ modalOpen: false });
    }

    handleModalData = (data) => {
        this.setState({ 
            modalData: data
        });
    }

    render() {
        let { products } = this.props;
        const { modalOpen } = this.state;
        return (
            <>
                <div className="related-products-area">
                    <div className="container">
                        <div className="section-title">
                            <h2><span className="dot"></span>Productos Relacionados</h2>
                        </div>

                        <div className="row">
                            {this.state.display ? <OwlCarousel 
                                className="trending-products-slides-two owl-carousel owl-theme"
                                {...options}
                            >
                                {products.map((data, idx) => (
                                    <div className="col-lg-12 col-md-12" key={idx}>
                                        <div className="single-product-box">
                                            <div className="product-image">
                                                <Link href="/product/[id]" as={`/product/${data.id}`}>
                                                    <a>
                                                         
                                                        <img src={data.imageHover} alt="image" />
                                                    </a>
                                                </Link>

                                                
                                            </div>

                                            <div className="product-content">
                                                <h3>
                                                    <Link href="/product/[id]" as={`/product/${data.id}`}>
                                                        <a>{data.title}</a>
                                                    </Link>
                                                </h3>

                                                <div className="product-price">
                                                        <span className="new-price">${data.price}</span>
                                                </div>

                                                
                                                <AddToCart {...data} />
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </OwlCarousel> : ''}
                        </div>
                    </div>
                </div>
                
                { modalOpen ? <QuickView 
                    closeModal={this.closeModal} 
                    modalData={this.state.modalData}
                /> : '' }
            </>
        );
    }
}

export default RelatedProducts