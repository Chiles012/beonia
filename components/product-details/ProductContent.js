import React, { Component } from 'react';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import Link from 'next/link';
import { addQuantityWithNumber } from '../../store/actions/actions';


class ProductContent extends Component {
    state = {
        qty: 1,
        max: 10,
        min: 1,
        sizeGuide: false,
        shipModal: false
    };

    handleAddToCartFromView = () => {
        this.props.addQuantityWithNumber(this.props.product.id, this.state.qty); 

        toast.success('Added to the cart', {
            position: "bottom-left",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
        });
    }

    IncrementItem = () => {
        this.setState(prevState => {
            if(prevState.qty < 10) {
                return {
                    qty: prevState.qty + 1
                }
            } else {
                return null;
            }
        });
    }

    DecreaseItem = () => {
        this.setState(prevState => {
            if(prevState.qty > 1) {
                return {
                    qty: prevState.qty - 1
                }
            } else {
                return null;
            }
        });
    }

    openSizeGuide = () => {
        this.setState({ sizeGuide: true });
    }

    closeSizeGuide = () => {
        this.setState({ sizeGuide: false });
    }

    openShipModal = () => {
        this.setState({ shipModal: true });
    }

    closeShipModal = () => {
        this.setState({ shipModal: false });
    }

    render() {
        const { sizeGuide, shipModal } = this.state;
        return (
            <>
                <div className="col-lg-6 col-md-6">
                    <div className="product-details-content">
                        <h3>{ this.props.product?.producto }</h3>

                        <div className="price">
                            <span className="new-price">$ { this.props.product?.precio }</span>
                        </div>

                        

                        <p> { this.props.product?.frase } </p>

                        <ul className="product-info">
                            <li><span>Tipo arreglo:</span> <Link href="#"><a>{this.props.product?.categoria}</a></Link></li>
                            
                        </ul>



                        <div className="product-add-to-cart">
                            <div className="input-counter">
                                <span 
                                    className="minus-btn"
                                    onClick={this.DecreaseItem}
                                >
                                    <i className="fas fa-minus"></i>
                                </span>
                                <input 
                                    type="text" 
                                    value={this.state.qty}
                                    min={this.state.min}
                                    max={this.state.max} 
                                    onChange={e => this.setState({ qty: e.target.value })}
                                />
                                <span 
                                    className="plus-btn"
                                    onClick={this.IncrementItem}
                                >
                                    <i className="fas fa-plus"></i>
                                </span>
                            </div>

                            <button 
                                type="submit" 
                                className="btn btn-primary"
                                onClick={() => {
                                    const carrito = JSON.parse(localStorage.getItem('carrito'));

                                    if(carrito) {
                                        const producto = carrito.find(producto => producto.id === this.props.product.id);

                                        if(producto) {
                                            carrito.push({...this.props.product, qty: this.state.qty});
                                        } else {
                                            carrito.push({
                                                ...this.props.product,
                                                qty: this.state.qty
                                            });
                                        }

                                        localStorage.setItem('carrito', JSON.stringify(carrito));
                                    } else {
                                        localStorage.setItem('carrito', JSON.stringify([{
                                            ...this.props.product,
                                            qty: this.state.qty
                                        }]));
                                    }
                                }}
                            >
                                <i 
                                    
                                    className="fas fa-cart-plus"
                                ></i> Añadir al carrito
                            </button>
                        </div>
                    </div>
                </div>
                { sizeGuide ? <SizeGuide 
                    closeSizeGuide={this.closeSizeGuide} 
                /> : '' }
                { shipModal ? <Shipping closeShipModal={this.closeShipModal} /> : '' }
            </>
        );
    }
}

const mapDispatchToProps= (dispatch)=>{
    return {
        addQuantityWithNumber: (id, qty) => {dispatch(addQuantityWithNumber(id, qty))}
    }
}

export default connect(
    null,
    mapDispatchToProps
)(ProductContent)