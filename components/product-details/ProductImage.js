import React, { Component } from 'react';
import Slider from "react-slick";
import Image from 'next/image';

class ProductImage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nav1: null,
            nav2: null
        };
    }

    componentDidMount() {
        this.setState({
            nav1: this.slider1,
            nav2: this.slider2
        });
    }

    renderSliderMainImages = () => {
        return DEFAULT_PROPS.map(({ id, image }) => {
            return (
                <div key={id}>
                    <div className="item">
                        <img src={image} alt="image" />
                    </div>
                </div>
            )
        })
    }

    renderSliderSubImages = () => {
        return DEFAULT_PROPS.map(({ id, image }) => {
            return (
                <div key={id}>
                    <div className="item">
                        <img src={image} alt="image" />
                    </div>
                </div>
            )
        })
    }

    render() {
        return (
            <div className="col-lg-6 col-md-6">
                <div className="products-page-gallery">
                    <div className="product-page-gallery-main">
                        <div className="product-image">
                            <img src={this.props.product?.foto} alt="image" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const DEFAULT_PROPS = [
    {
        id: 1,
        image: '/images/Products/Condolencias/img1.jpg'
    },
    {
        id: 2,
        image: '/images/Products/Condolencias/img1.jpg'
    },
    {
        id: 3,
        image: '/images/Products/Condolencias/img1.jpg'
    },
    {
        id: 4,
        image: '/images/Products/Condolencias/img1.jpg'
    },
    {
        id: 5,
        image: '/images/Products/Condolencias/img1.jpg'
    },
    {
        id: 6,
        image: '/images/Products/Condolencias/img1.jpg'
    },
    {
        id: 7,
        image: '/images/Products/Condolencias/img1.jpg'
    }
];

export default ProductImage;
