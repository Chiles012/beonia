import React, { Component } from 'react';
import Link from 'next/link';

class ProductCategories extends Component {
    render() {
        return (
            <section className="category-boxes-area pt-60">
                <div className="container">
                    <div className="section-title">
                        <h2><span className="dot"></span> Para todas la ocaciones!</h2>
                    </div>

                    <div className="row">
                        <div className="col-lg-4 col-sm-6">
                            <div className="category-boxes">
                                <img src="/images/category-product-image/cp-img1.jpg" alt="image" />
                                <div className="content">
                                    <h3>Cumpleaños</h3>
                                    <span>165 Products</span>

                                    <Link href="#">
                                        <a className="shop-now-btn">Compra Ahora</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="category-boxes">
                                <img src="/images/category-product-image/cp-img2.jpg" alt="image" />
                                <div className="content">
                                    <h3>Amor/Aniversario</h3>
                                    <span>165 Products</span>
                                    
                                    <Link href="#">
                                        <a className="shop-now-btn">Compra Ahora</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-sm-6">
                            <div className="category-boxes">
                                <img src="/images/category-product-image/cp-img3.jpg" alt="image" />
                                <div className="content">
                                    <h3>Graduación</h3>
                                    <span>165 Products</span>
                                    
                                    <Link href="#">
                                        <a className="shop-now-btn">Compra Ahora</a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-sm-6">
                            <div className="category-boxes">
                                <img src="/images/category-product-image/cp-img3.jpg" alt="image" />
                                <div className="content">
                                    <h3>Para Todos</h3>
                                    <span>165 Products</span>
                                    
                                    <Link href="#">
                                        <a className="shop-now-btn">Compra Ahora</a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ProductCategories;