import React from 'react';
import Head from 'next/head';
import GoTop from '../Shared/GoTop';
import { ToastContainer, Slide } from 'react-toastify';
import ReactTooltip from 'react-tooltip'

const Layout = ({ children }) => {
    return(
        <>
            <ReactTooltip  />
            
            <Head>
                <title>BEONIA</title>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                <meta name="description" content="BEONIA" />
                <meta name="og:title" property="og:title" content="BEONIA"></meta>
                <meta name="twitter:card" content="BEONIA"></meta>
                <link rel="icon" type="image/png" href="/images/MG2@4x.png"></link>
                <link rel="canonical" href="BEONIA"></link>
                <meta property="og:image"  content="BEONIA" />
            </Head>

            { children }

            <ToastContainer transition={Slide} />

            <GoTop scrollStepInPx="100" delayInMs="10.50" />
        </>
    );
}
export default Layout;