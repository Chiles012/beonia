import { initializeApp } from 'firebase/app';

const app = initializeApp({
    apiKey: "AIzaSyAUlt_fQ5mP8u8JgD7yMICksmZfc3DzoAo",
    authDomain: "floreriabeonia.firebaseapp.com",
    projectId: "floreriabeonia",
    storageBucket: "floreriabeonia.appspot.com",
    messagingSenderId: "146799618306",
    appId: "1:146799618306:web:c49377617c650542b6ba1b",
    measurementId: "G-2MYKJT5SP1"
});

export {
    app
}