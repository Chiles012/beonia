import React, { Component } from 'react';
import Link from 'next/link';
import Navbar from '../components/Layout/Navbar';
import Footer from '../components/Layout/Footer';
import Facility from '../components/Common/Facility';
import Breadcrumb from '../components/Common/Breadcrumb';

class Custom404 extends Component {
    render() {
        return (
            <>
                <Navbar />

                <Breadcrumb title="404 Not Found" />

                <section className="error-area ptb-60">
                    <div className="container">
                        <div className="error-content">
                            <img src="/images/404.png" alt="error" />

                            <h3>{this.props.statusCode} Pagina No encontrada</h3>
                            <p>La pagina que estas buscando no sirve</p>

                            <Link href="/">
                                <a className="btn btn-light">Ir al Inicio</a>
                            </Link>
                        </div>
                    </div>
                </section>

                <Facility />
                
                <Footer />
            </>
        );
    }
}

export default Custom404;