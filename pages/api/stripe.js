import Stripe from "stripe";

export default async function handler(req, res) {

    const stripe = new Stripe("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");

    let { products, total } = req.body;

    products = products.map(product => {
        return {
            price_data: {
                currency: 'mxn',
                product_data: {
                    name: product.producto,
                },
                unit_amount: product.precio * 100,
                product_data: {
                    name: product.producto,
                    images: [product.foto]
                }
            },
            quantity: product.qty,

        }
    })

    const session = await stripe.checkout.sessions.create({
        payment_method_types: ['card', 'oxxo'],
        shipping_address_collection: {
            allowed_countries: ['MX'],
        },
        line_items: [
            ...products,
        ],
        mode: 'payment',
        allow_promotion_codes: true,
        success_url: 'http://localhost:3000/success',
        cancel_url: 'http://localhost:3000/cancel',

    });

    console.log(session.url);

    res.send({ url: session.url });
}
