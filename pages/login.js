import React, { Component } from 'react';
import Link from 'next/link';
import Navbar from '../components/Layout/Navbar';
import Footer from '../components/Layout/Footer';
import Facility from '../components/Common/Facility';
import Breadcrumb from '../components/Common/Breadcrumb';

const Login = () => {
    return (
        <>
            <Navbar />

            <Breadcrumb title="Login" />

            <section className="login-area ptb-60">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-12">
                            <div className="login-content">
                                <div className="section-title">
                                    <h2><span className="dot"></span> Ingresar</h2>
                                </div>

                                <form className="login-form">
                                    <div className="form-group">
                                        <label>Correo</label>
                                        <input type="email" className="form-control" placeholder="Enter your name" id="name" name="name" />
                                    </div>

                                    <div className="form-group">
                                        <label>Contraseña</label>
                                        <input type="password" className="form-control" placeholder="Enter your password" id="password" name="password" />
                                    </div>

                                    <button type="submit" className="btn btn-primary">Ingresar</button>
                                    
                                    <Link href="#">
                                        <a className="forgot-password">Perdiste tu contraseña?</a>
                                    </Link>
                                </form>
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-12">
                            <div className="new-customer-content">
                                <div className="section-title">
                                    <h2><span className="dot"></span> Nuevo cliente</h2>
                                </div>

                                <span>Crear cuenta</span>
                                <p>nicie una cuenta gratis en nuestra tienda. El registro es rápido y fácil. Le permite poder realizar pedidos en nuestra tienda. Para comenzar a comprar, haga clic en registrarse.

</p>
                                <Link href="/signup">
                                    <a className="btn btn-light">Crear cuenta</a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <Facility />
            
            <Footer />
        </>
    );
}

export default Login;
