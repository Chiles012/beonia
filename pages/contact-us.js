import React, { Component } from 'react';
import Navbar from '../components/Layout/Navbar';
import Footer from '../components/Layout/Footer';
import Facility from '../components/Common/Facility';
import Breadcrumb from '../components/Common/Breadcrumb';

class Index extends Component {
    render() {
        return (
            <>
                <Navbar />
                
                <Breadcrumb title="Contact Us" />

                <section className="contact-area ptb-60">
                    <div className="container">
                        <div className="section-title">
                            <h2><span className="dot"></span> Contact Us</h2>
                        </div>

                        <div className="row">
                            <div className="col-lg-5 col-md-12">
                                <div className="contact-info">
                                    <h3>Contactos</h3>
                                    <p>Tienes una duda?,  contactanos para poder ayudarte</p>

                                    <ul className="contact-list">
                                        
                                        <li><i className="fas fa-phone"></i> LLamanos <a href="#">(477) 379-7898</a></li>
                                        <li><i className="far fa-envelope"></i> Email: <a href="#">support@correo.com</a></li>
                                        <li><i className="fas fa-fax"></i> whatssapp: <a href="#">+477123456</a></li>
                                    </ul>

                                    <h3>Horas de atencion:</h3>
                                    <ul className="opening-hours">
                                        <li><span>Lunes:</span> 8AM - 6PM</li>
                                        <li><span>Martes:</span> 8AM - 6PM</li>
                                        <li><span>Miercoles:</span> 8AM - 6PM</li>
                                        <li><span>Jueves - Friday:</span> 8AM - 6PM</li>
                                        <li><span>Viernes:</span> Closed</li>
                                    </ul>

                                    <h3>Siguenos:</h3>
                                    <ul className="social">
                                        <li><a href="#"><i className="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i className="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i className="fab fa-instagram"></i></a></li>
                                        <li><a href="#"><i className="fab fa-behance"></i></a></li>
                                        <li><a href="#"><i className="fab fa-skype"></i></a></li>
                                        <li><a href="#"><i className="fab fa-pinterest-p"></i></a></li>
                                        <li><a href="#"><i className="fab fa-youtube"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </section>
                
                <Facility />
                
                <Footer />
            </>
        );
    }
}

export default Index;