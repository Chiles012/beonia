import React, { useEffect } from 'react';
import { useRouter } from 'next/router'
import Navbar from '../../components/Layout/Navbar';
import Breadcrumb from '../../components/Common/Breadcrumb';
import Footer from '../../components/Layout/Footer';
import ProductImage from '../../components/product-details/ProductImage';
import ProductContent from '../../components/product-details/ProductContent';
import { collection, getDocs, getFirestore } from 'firebase/firestore';
import { app } from '../../config/firebaseConfig';


const Product = () => {
    const router = useRouter()
    const { id } = router.query
    const [product, setProduct] = React.useState({})

    useEffect(() => {
        const dbInstance = getFirestore(app);
        const db = collection(dbInstance, 'Inventario');

        getDocs(db).then(docs => {
            const products = []
            docs.docs.forEach(doc => {
                const data = doc.data()
                data.id = doc.id
                products.push(data)
            })

            const product = products.find(product => product.producto === id)
            console.log(product)
            setProduct(product)
        }).catch(err => {
            console.log(err)
            setProduct({})
        })
    }, [id])

    return (
        <>
            <Navbar />
            <Breadcrumb title={product?.producto} />

            <section className="products-details-area pt-60">
                <div className="container">
                    <div className="row">
                        <ProductImage product={product} />
                        <ProductContent product={product} />
                    </div>
                </div>
                
            </section>

            <Footer />
        </>
    );
}

export default Product