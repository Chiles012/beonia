import React, { Component } from 'react';
import Navbar from '../components/Layout/Navbar';
import Footer from '../components/Layout/Footer';
import Facility from '../components/Common/Facility';
import Breadcrumb from '../components/Common/Breadcrumb';


class Index extends Component {
    render() {
        return (
            <>
                <Navbar />

                <Breadcrumb title="About Us" />

                <section className="about-area ptb-60">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-6 col-md-12">
                                <div className="about-content">
                                    <h2>¿Quiénes somos?</h2>
                                    <p>Somos amor, pasión, frescura, creatividad y romance.
                                        Beonia tiene su inspiración en las artes… Fusiona la poesía y letras con el sorprendente mundo de las flores, lo que da como resultado las creaciones que aquí encontraras.
                                        Cada arreglo lleva el nombre de un ilustre y cada nota va sellada con lacre, lo que le da un toque confidencial, original y exclusivo a tus mensajes.
                                        Nuestros proveedores nos entregan flores de temporada frescas y recién cortadas, por lo que te garantizamos calidad y buen gusto en cada una de tus compras y servicios contratados.
                                        Aquí encontrarás un grupo de profesionales que escucha, lee, atiende y comprende tus necesidades. No sólo estás regalando flores, estas regalando emociones, significados; son un símbolo de amor, amistad, deseo, agradecimiento, respeto o lealtad.
                                    </p>

                                    <p>¡Acércate con nosotros y conoce más de Beonia en nuestras redes sociales! ¡Trabajamos para ti!</p>


                                    <div className="signature mb-0">
                                        <img src="/images/signature.png" alt="image" />
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-12">
                                <div className="about-image">
                                    <img src="/images/about1.jpg" className="about-img1" alt="image" />
                                    <img src="/images/about2.jpg" className="about-img2" alt="image" />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>



                <Facility />

                <Footer />
            </>
        );
    }
}

export default Index;
