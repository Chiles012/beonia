import React from 'react';
import { useSelector } from 'react-redux'
import Navbar from '../components/Layout/Navbar';
import BannerSlider from '../components/shop-style-six/BannerSlider';
import ProductCategories from '../components/shop-style-six/ProductCategories';
import Facility from '../components/Common/Facility';
import Footer from '../components/Layout/Footer';
import AddsModal from '../components/Modal/AddsModal';

const ShopStyleSix = () => {
    const products = useSelector((state) => state.productsCollectionSix)
    const addedItemsToCompare = useSelector((state) => state.addedItemsToCompare)
    return (
        <>
            <Navbar />

            <BannerSlider />

            <ProductCategories />

            <Facility />

            <Footer />
            
            <AddsModal />
        </>
    );
}

export default ShopStyleSix;