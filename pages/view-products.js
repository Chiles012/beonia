import React, { Component } from 'react';
import { useSelector } from 'react-redux'
import Navbar from '../components/Layout/Navbar';
import Footer from '../components/Layout/Footer';
import Breadcrumb from '../components/Common/Breadcrumb';
import Facility from '../components/Common/Facility';
import ProductsFilterOptions from '../components/Common/ProductsFilterOptions';
import ProductsCard from '../components/products/ProductsCard';
import { app } from '../config/firebaseConfig';
import { collection, getDocs, getFirestore } from 'firebase/firestore';

const hookClass = (props) => {
    const products = useSelector((state) => state.products)
    const addedItemsToCompare = useSelector((state) => state.addedItemsToCompare)
    return <Index {...props} products={products} CompareProducts={addedItemsToCompare} />
}

class Index extends Component {

    state = {
        gridClass: '',
        products: [],
    } 

    handleGrid = (e) => {
        this.setState({
            ...this.state,
            gridClass: e
        });
    }

    componentDidMount() {
        const dbInstance = getFirestore(app);
        const db = collection(dbInstance, 'Inventario');

        getDocs(db).then(docs => {
            const products = []
            docs.docs.forEach(doc => {
                const data = doc.data()
                data.id = doc.id
                products.push(data)
            })
            this.setState({
                ...this.state,
                products
            })
        }).catch(err => {
            console.log(err)
            this.setState({
                ...this.state,
                products: []
            })
        })
    }

    render() {
        return (
            <>
                <Navbar />
                
                <Breadcrumb title="Condolencias" />

                <section className="products-collections-area ptb-60">
                    <div className="container">
                        <div className="section-title">
                            <h2><span className="dot"></span> Condolencias</h2>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <ProductsFilterOptions onClick={this.handleGrid} />
                                <div id="products-filter" className={`products-collections-listing row ${this.state.gridClass}`}>
                                    <ProductsCard products={this.state.products} />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <Facility />

                <Footer />
            </>
        );
    }
}

export default hookClass;
